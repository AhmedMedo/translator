<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslationConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translation_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('from_language_id');
            $table->integer('to_language_id');
            $table->string('num_of_words_per_page')->nullable();
            $table->string('price_per_page')->nullable();
            $table->boolean('active')->default(1);
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translation_configs');
    }
}
