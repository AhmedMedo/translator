<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditClientRequestCommisionMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_translation_requests', function (Blueprint $table) {
            //
            $table->string('request_number')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('translator_commission')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_translation_requests', function (Blueprint $table) {
            //
        });
    }
}
