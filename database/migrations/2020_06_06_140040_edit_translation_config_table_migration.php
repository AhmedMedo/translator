<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditTranslationConfigTableMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('translation_configs', function (Blueprint $table) {
            //
            $table->string('price_per_page_aed')->nullable();
            $table->string('price_per_page_chinese')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('translation_configs', function (Blueprint $table) {
            //
        });
    }
}
