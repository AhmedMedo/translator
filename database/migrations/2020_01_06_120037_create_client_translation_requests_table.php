<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientTranslationRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_translation_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('translation_package_id');
            $table->string('doc_path')->nullable();
            $table->string('translated_doc_path')->nullable();
            $table->string('doc_num_of_words')->nullable();
            $table->double('price')->default(0);
            $table->integer('status_id');
            $table->boolean('is_paid')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_translation_requests');
    }
}
