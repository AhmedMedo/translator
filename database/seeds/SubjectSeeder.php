<?php

use Illuminate\Database\Seeder;
use App\Models\Subject;
class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
          Subject::truncate();
          Subject::firstOrCreate(['name' => 'Technical','name_ar'=>'تقني','name_zh'=>'技術']);
          Subject::firstOrCreate(['name' => 'Medical','name_ar'=>'طبي','name_zh'=>'醫療類']);
          Subject::firstOrCreate(['name' => 'Legal','name_ar'=>'قانوني','name_zh'=>'法律']);
          Subject::firstOrCreate(['name' => 'Others','name_ar'=>'اخرى','name_zh'=>'其他']);

//        Subject::firstOrCreate(['name' => 'Agriculture']);
//        Subject::firstOrCreate(['name' => 'Transportation & logistics']);
//        Subject::firstOrCreate(['name' => 'Transport']);
//        Subject::firstOrCreate(['name' => 'Tourism']);
//        Subject::firstOrCreate(['name' => 'Telecommunications']);
//        Subject::firstOrCreate(['name' => 'Technology']);
//        Subject::firstOrCreate(['name' => 'Sovereign investment funds']);
//        Subject::firstOrCreate(['name' => 'Social Sciences']);
//        Subject::firstOrCreate(['name' => 'Private equity']);
//        Subject::firstOrCreate(['name' => 'Pharmaceuticals']);
//        Subject::firstOrCreate(['name' => 'Other']);
//        Subject::firstOrCreate(['name' => 'Natural Sciences']);
//        Subject::firstOrCreate(['name' => 'Military']);
//        Subject::firstOrCreate(['name' => 'Medical']);
//        Subject::firstOrCreate(['name' => 'Media']);
//        Subject::firstOrCreate(['name' => 'Materials']);
//        Subject::firstOrCreate(['name' => 'Leisure']);
//        Subject::firstOrCreate(['name' => 'Legal']);
//        Subject::firstOrCreate(['name' => 'Insurance']);
//        Subject::firstOrCreate(['name' => 'Industrial manufacturing']);
//        Subject::firstOrCreate(['name' => 'Human Sciences']);
//        Subject::firstOrCreate(['name' => 'Hospitality & leisure']);
//        Subject::firstOrCreate(['name' => 'Healthcare']);
//        Subject::firstOrCreate(['name' => 'Government & public services']);
//        Subject::firstOrCreate(['name' => 'General']);
//        Subject::firstOrCreate(['name' => 'Gaming']);
//        Subject::firstOrCreate(['name' => 'Forest, paper & packaging']);
//        Subject::firstOrCreate(['name' => 'Financial services']);
//        Subject::firstOrCreate(['name' => 'Environment']);
//        Subject::firstOrCreate(['name' => 'Engineering']);
//        Subject::firstOrCreate(['name' => 'Energy, utilities & resources']);
//        Subject::firstOrCreate(['name' => 'Energy']);
//        Subject::firstOrCreate(['name' => 'Electronics']);
//        Subject::firstOrCreate(['name' => 'Education']);
//        Subject::firstOrCreate(['name' => 'Economics']);
//        Subject::firstOrCreate(['name' => 'Consumer markets']);
//        Subject::firstOrCreate(['name' => 'Computers']);
//        Subject::firstOrCreate(['name' => 'Capital projects & infrastructure']);
//        Subject::firstOrCreate(['name' => 'Banking and Finance']);
//        Subject::firstOrCreate(['name' => 'Banking & capital markets']);
//        Subject::firstOrCreate(['name' => 'Automotive']);
//        Subject::firstOrCreate(['name' => 'Asset & wealth management']);
//        Subject::firstOrCreate(['name' => 'Arts and Culture']);
//        Subject::firstOrCreate(['name' => 'Aerospace, defence & security']);
    }
}
