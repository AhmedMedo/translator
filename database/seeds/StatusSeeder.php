<?php

use Illuminate\Database\Seeder;
use App\Models\Status;
class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Status::firstOrCreate(['name' => 'new']);
        Status::firstOrCreate(['name' => 'seen']);
        Status::firstOrCreate(['name' => 'rejected']);
        Status::firstOrCreate(['name' => 'under_translation']);
        Status::firstOrCreate(['name' => 'finished']);
    }
}
