@extends('admin.layouts.master')
@section('title','Clients Requests')
@push('styles')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Clients Requests
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_3">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Client Name</th>
                    <th>Language</th>
                    <th>Number Of Words</th>
                    <th>Price</th>
                    <th>Subject</th>
                    <th>Status</th>
                    <th>Requestd At</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($client_requests as $client_request)
                    <tr>
                        <td>{{$client_request->id}}</td>
                        <td>{{@$client_request->client->name}}</td>
                        <td>
                           From {{@$client_request->package->FromLanguage->name}} To {{@$client_request->package->ToLanguage->name}}
                        </td>
                        <td>{{$client_request->doc_num_of_words}}</td>
                        <td>{{$client_request->price}}</td>
                        <td>{{@$client_request->subject->name}}</td>
                        <td>{{@$client_request->status->name}}</td>
                        <td>{{$client_request->created_at}}</td>
                        <td>
                            <a href="{{route('admin.client_requests.show',$client_request->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                                <i class="la la-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>

@endsection

@push('scripts')
    <!--begin::Page Vendors(used by this page) -->
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>

    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/pages/crud/datatables/data-sources/html.js')}}" type="text/javascript"></script>

    <!--end::Page Scripts -->

@endpush
