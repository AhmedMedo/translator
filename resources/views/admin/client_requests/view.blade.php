@extends('admin.layouts.master')
@section('title','View Request')
@push('styles')
    <link href="{{asset('assets/css/pages/invoices/invoice-1.css')}}" rel="stylesheet" type="text/css"/>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-invoice-1">
                        <div class="kt-invoice__head"
                             style="background-image: url({{asset('assets/media/bg/bg-6.jpg')}});">
                            <div class="kt-invoice__container">
                                <div class="kt-invoice__brand">
                                    <h1 class="kt-invoice__title">Status : {{@$client_request->status->name}}</h1>
                                </div>
                                <div class="kt-invoice__items">
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">DATE</span>
                                        <span class="kt-invoice__text">{{$client_request->created_at}}</span>
                                    </div>
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">Client Name</span>
                                        <span class="kt-invoice__text">{{@$client_request->client->name}}</span>
                                    </div>
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">Client Email</span>
                                        <span class="kt-invoice__text">{{@$client_request->client->email}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-invoice__body">
                            <div class="kt-invoice__container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>SOURCE LANGUAGE</th>
                                            <th>TARGET LANGUAGE</th>
                                            <th>NUMBER OF WORDS</th>
                                            <th>SUBJECT</th>
                                            <th>PRICE</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{@$client_request->package->FromLanguage->name}}</td>
                                            <td>{{@$client_request->package->ToLanguage->name}}</td>
                                            <td>{{@$client_request->doc_num_of_words}}</td>
                                            <td>{{@$client_request->subject->name}}</td>
                                            <td>{{$client_request->price}} AED</td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="kt-invoice__actions">
                            <div class="kt-invoice__container">
                                <a href="{{route('request.download',$client_request->id)}}" class="btn btn-label-brand btn-bold">
                                    Download Client File
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Assign Translator
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" method="POST" enctype="multipart/form-data"  action="{{route('admin.client_requests.assign',$client_request->id)}}">
                    @csrf
                    <div class="kt-portlet__body">
                        <div class="form-group">
                            <label for="exampleSelectd">Select Translator</label>
                            <select class="form-control" name="translator_id" id="exampleSelectd">
                                @foreach($translators as $translator)
                                    <option value="{{$translator->id}}">{{$translator->name}}</option>
                                @endforeach
                            </select>
                        </div>
{{--                        <div class="form-group row">--}}
{{--                            <div class="col-lg-12 col-md-12 col-sm-12">--}}
{{--                                <div class="dropzone dropzone-default" id="kt_dropzone_1">--}}
{{--                                    <div class="dropzone-msg dz-message needsclick">--}}
{{--                                        <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>--}}
{{--                                        <span class="dropzone-msg-desc">This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-success" id="assgin_btn">Submit</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/pages/crud/file-upload/dropzonejs.js')}}" type="text/javascript"></script>
    <!--end::Page Scripts -->
    <script>
        $('#kt_dropzone_1').dropzone({
            url: "{{route('admin.client_requests.assign',$client_request->id)}}", // Set the url for your upload script location
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 5, // MB
            addRemoveLinks: true,
            autoProcessQueue:false,
            accept: function(file, done) {
                if (file.name == "justinbieber.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            },
            init:function () {
                var submitButton = document.querySelector("#assgin_btn");
                myDropzone = this;
                submitButton.addEventListener("click", function(e) {
                    e.preventDefault();
                    myDropzone.processQueue();

                });
            }
        });
    </script>
@endpush
