@extends('admin.layouts.master')
@section('title','Translators')
@push('styles')

@endpush

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Translator & Agencies
                        </h3>
                    </div>
                </div>
                @if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!--begin::Form-->
                {!! isset($translator) ? Form::model($translator,['url'=>route('translators.update',[$translator->id]) , 'method'=>'PUT','class'=>'kt-form kt-form--label-right']) : Form::open(['route' => 'translators.store', 'method' => 'post','class'=>'kt-form kt-form--label-right']) !!}
                <div class="kt-portlet__body">

                    <div class="form-group row">
                        <div class="col-lg-6">
                            {!! Form::label(' Translator \ Agency Name', 'Translator \ Agency Name', ['class' => 'control-label']) !!}
                            {!! Form::text('name', null, ['class' => 'form-control' ,'required'=>'required']) !!}
                        </div>
                        <div class="col-lg-6">
                            {!! Form::label('Email', 'Email', ['class' => 'control-label']) !!}
                            {!! Form::email('email', null, ['class' => 'form-control '.($errors->has('email') ? 'is-invalid':'')]) !!}
                            @if($errors->has('email'))
                            <div class="invalid-feedback">{{ $errors->first('email') }}.</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            {!! Form::label('Password', 'Password', ['class' => 'control-label']) !!}
                            @if(isset($translator))
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                            @else
                                {!! Form::password('password', ['class' => 'form-control' ,'required'=>'required']) !!}

                            @endif
                            @if($errors->has('password'))
                                <div class="invalid-feedback">{{ $errors->first('password') }}.</div>
                            @endif

                        </div>
                        <div class="col-lg-6">
                            {!! Form::label('Confirm Password', 'Confirm Password', ['class' => 'control-label']) !!}
                            @if(isset($translator))
                                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                            @else
                                {!! Form::password('password_confirmation', ['class' => 'form-control' ,'required'=>'required']) !!}

                            @endif
                            @if($errors->has('password_confirmation'))
                                <div class="invalid-feedback">{{ $errors->first('password') }}.</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            {!! Form::label('Percentage', 'Percentage', ['class' => 'control-label']) !!}
                            {!! Form::number('percentage', null, ['class' => 'form-control' ,'required'=>'required']) !!}

                        </div>
                        <div class="col-lg-6">
                            {!! Form::label('Langauge Pairs', 'Langauge Pairs', ['class' => 'control-label']) !!}
                            <select class="form-control kt-select2" id="kt_select2_2" name="packages[]" multiple>
                                @foreach($pairs as $pair)
                                    <option value="{{$pair->id}}"
                                            @if(isset($translator))
                                            @if(in_array($pair->id , $translator->packages()->pluck('translation_config_id')->toArray()))
                                            selected

                                        @endif

                                        @endif

                                    >
                                        From {{$pair->FromLanguage->name}} to {{ $pair->ToLanguage->name }}</option>
                                @endforeach

                            </select>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="kt-checkbox-inline">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                {!! Form::checkbox('active', '1', null,  ['id' => 'active']) !!} Active
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-6">
                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            </div>
                            <div class="col-lg-6 kt-align-right">
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}

            <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{asset('assets/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>
@endpush
