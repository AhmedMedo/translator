@extends('admin.layouts.master')
@section('title','Translators')
@push('styles')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Translators
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{route('translators.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            New Translator
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_2">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Languages</th>
                    <th>Creation Date</th>
                    <th>Percentage</th>
                    <th>Active</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($translators as $translator)
                    <tr>
                        <td>{{$translator->id}}</td>
                        <td>{{@$translator->name}}</td>
                        <td>{{@$translator->email}}</td>
                        <td>
                            @if($translator->packages)
                                @foreach($translator->packages as $package)
                                    {{@$package->FromLanguage->name}} To {{@$package->ToLanguage->name}}
                                    <br>

                                @endforeach

                             @endif
                        </td>
                        <td>{{$translator->created_at}}</td>
                        <td>{{$translator->percentage}} %</td>
                        <td>{{$translator->active}}</td>
                        <td>
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="{{route('translators.edit',$translator->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                                        <i class="la la-edit"></i>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <form action="{{route('translators.destroy',$translator->id)}}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="delete">
                                        <button type="submit" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"><i class="la la-remove"></i></button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>

@endsection

@push('scripts')
    <!--begin::Page Vendors(used by this page) -->
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>

    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/pages/crud/datatables/data-sources/html.js')}}" type="text/javascript"></script>

    <!--end::Page Scripts -->

@endpush
