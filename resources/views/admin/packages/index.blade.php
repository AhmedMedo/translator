@extends('admin.layouts.master')
@section('title','Packages')
@push('styles')
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    Language Pair Prices
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{route('packages.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            New Language
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>From Language</th>
                    <th>To Language</th>
                    <th>Number Of words per page</th>
                    <th>Price Per page ($)</th>
                    <th>Price Per page (AED)</th>
                    <th>Price Per page (¥)</th>
                    <th>Subject</th>
                    <th>Creation Date</th>
                    <th>Active</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($packages as $package)
                <tr>
                    <td>{{$package->id}}</td>
                    <td>{{@$package->FromLanguage->name}}</td>
                    <td>{{@$package->ToLanguage->name}}</td>
                    <td>{{$package->num_of_words_per_page}}</td>
                    <td>{{$package->price_per_page}}</td>
                    <td>{{$package->price_per_page_aed}}</td>
                    <td>{{$package->price_per_page_chinese}}</td>
                    <td>{{@$package->subject->name}}</td>
                    <td>{{$package->created_at}}</td>
                    <td>{{$package->active}}</td>
                    <td>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{route('packages.edit',$package->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                                    <i class="la la-edit"></i>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <form action="{{route('packages.destroy',$package->id)}}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <button type="submit" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete"><i class="la la-remove"></i></button>
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
                 @endforeach
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>

@endsection

@push('scripts')
    <!--begin::Page Vendors(used by this page) -->
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>

    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/pages/crud/datatables/data-sources/html.js')}}" type="text/javascript"></script>

    <!--end::Page Scripts -->

@endpush
