@extends('admin.layouts.master')
@section('title','Create Packages')
@push('styles')

@endpush

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Add Language Pricing
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                {!! isset($package) ? Form::model($package,['url'=>route('packages.update',[$package->id]) , 'method'=>'PUT','class'=>'kt-form kt-form--label-right']) : Form::open(['route' => 'packages.store', 'method' => 'post','class'=>'kt-form kt-form--label-right']) !!}

                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                {!! Form::label('Source Languages', 'Source Languages', ['class' => 'control-label']) !!}
                                 {!! Form::select('from_language_id', $langauges , null , ['class' => 'form-control kt-select2','id'=>"kt_select2_1",'requierd']) !!}
                                <span class="form-text text-muted">Select Source Language</span>
                            </div>
                            <div class="col-lg-6">
                                {!! Form::label('Target Languages', 'Target Languages', ['class' => 'control-label']) !!}
                                {!! Form::select('to_language_id', $langauges , null , ['class' => 'form-control kt-select2','id'=>"kt_select2_2"]) !!}
                                <span class="form-text text-muted">Select Target Language</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                {!! Form::label('Number of words per page', 'Number of words per page', ['class' => 'control-label']) !!}
                                {!! Form::number('num_of_words_per_page', null, ['class' => 'form-control' ,'required'=>'required']) !!}
                            </div>
                            <div class="col-lg-6">
                                {!! Form::label('Price Per Page USD', 'Price Per Page USD ', ['class' => 'control-label']) !!}
                                {!! Form::number('price_per_page', null, ['class' => 'form-control','required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                {!! Form::label('Price Per Page AED', 'Price Per Page AED', ['class' => 'control-label']) !!}
                                {!! Form::number('price_per_page_aed', null, ['class' => 'form-control' ,'required'=>'required']) !!}
                            </div>
                            <div class="col-lg-6">
                                {!! Form::label('Price Per Page Chinese', 'Price Per Page Chinese', ['class' => 'control-label']) !!}
                                {!! Form::number('price_per_page_chinese', null, ['class' => 'form-control','required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-12">
                                {!! Form::label('Subject', 'Subject', ['class' => 'control-label']) !!}
                                {!! Form::select('subject_id', $subjects , null , ['class' => 'form-control kt-select2','id'=>"kt_select2_3",'requierd']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                                <div class="kt-checkbox-inline">
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                        	{!! Form::checkbox('active', '1', null,  ['id' => 'active']) !!} Make the Pair Active
                                        <span></span>
                                    </label>
                                </div>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-6">
                                    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                </div>
                                <div class="col-lg-6 kt-align-right">
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{asset('assets/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>
@endpush
