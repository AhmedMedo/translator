@extends('clients.layouts.master')
@section('title','Order Request')
@push('styles')

@endpush

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{trans('dashboard.order_translation')}}
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                {!! Form::open(['route' => 'my_requests.store', 'method' => 'post','class'=>'kt-form kt-form--label-right']) !!}

                <div class="kt-portlet__body">
                    <div class="form-group row">
                        <div class="col-lg-4">
                            {!! Form::label(trans('dashboard.source_language'), trans('dashboard.source_language'), ['class' => 'control-label']) !!}
                            {!! Form::select('from_language_id', $source_languages , null , ['class' => 'form-control kt-select2','id'=>"kt_select2_1",'required']) !!}
                            <span class="form-text text-muted">{{trans('dashboard.select_source_language')}}</span>
                        </div>
                        <div class="col-lg-4">
                            {!! Form::label(trans('dashboard.target_language'), trans('dashboard.target_language'), ['class' => 'control-label']) !!}
                            {!! Form::select('to_language_id',$target_languages , null , ['class' => 'form-control kt-select2','id'=>"kt_select2_2",'required']) !!}
                            <span class="form-text text-muted">{{trans('dashboard.select_target_language')}}</span>
                        </div>
                        <div class="col-lg-4">
                            {!! Form::label(trans('dashboard.sector'), trans('dashboard.sector'), ['class' => 'control-label']) !!}
                            {!! Form::select('subject_id', $subjects , null , ['class' => 'form-control kt-select2','id'=>"kt_select2_3",'requierd']) !!}
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="dropzone dropzone-default" id="kt_dropzone_2">
                                <div class="dropzone-msg dz-message needsclick">
                                    <h3 class="dropzone-msg-title">{{trans('dashboard.upload')}}.</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($errors->has('file_path'))
                        <div class="invalid-feedback" style="display: block;font-size: 15px">{{$errors->first('file_path')}}</div>
                    @endif
                    {!! Form::hidden('file_path', NULL, ['id' => 'file_path']) !!}
                    <div class="form-group row">
                        <div class="col-lg-3">
                            {!! Form::label(trans('dashboard.word_count'),trans('dashboard.word_count'), ['class' => 'control-label']) !!}
                            {!! Form::number('file_words_count', null, ['placeholder'=>'-','class' => 'form-control' ,'readonly','id'=>'words_count']) !!}
                        </div>
                        <div class="col-lg-3">
                            @php
                                // if(\Session::get('currency') =='usd')
                                //   {
                                //       $currency = '$';
                                //   }
                                // elseif (\Session::get('currency') == 'aed')
                                // {
                                //     $currency = 'AED';

                                // }else{
                                //     $currency= '¥';

                                // }

                                $currency = 'AED';

                            @endphp

                            {!! Form::label(trans('dashboard.total_price'), trans('dashboard.total_price').'('.$currency.')', ['class' => 'control-label']) !!}
                            {!! Form::number('price', null, ['placeholder'=>'-','class' => 'form-control','readonly','id'=>'price']) !!}
                        </div>
                        <div class="col-lg-3">
                            {!! Form::label(trans('dashboard.num_of_pages'),trans('dashboard.num_of_pages'), ['class' => 'control-label']) !!}
                            {!! Form::number('num_of_pages', null, ['placeholder'=>'-','class' => 'form-control' ,'readonly','id'=>'num_of_pages']) !!}
                        </div>
                        <div class="col-lg-3">
                            {!! Form::label(trans('dashboard.total_houres'),trans('dashboard.total_houres'), ['class' => 'control-label']) !!}
                            {!! Form::number('total_houres', null, ['placeholder'=>'-','class' => 'form-control' ,'readonly','id'=>'total_houres']) !!}

                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            {!! Form::label(trans('dashboard.first_name'), trans('dashboard.first_name'), ['class' => 'control-label']) !!}
                            {!! Form::text('first_name', null, ['placeholder'=>trans('dashboard.first_name'),'class' => 'form-control'],'requierd') !!}
                        </div>
                        <div class="col-lg-3">
                            {!! Form::label(trans('dashboard.last_name'), trans('dashboard.last_name'), ['class' => 'control-label']) !!}
                            {!! Form::text('last_name', null, ['placeholder'=>trans('dashboard.last_name'),'class' => 'form-control','requierd']) !!}
                        </div>
                        <div class="col-lg-3">
                            {!! Form::label(trans('dashboard.address'), trans('dashboard.address'), ['class' => 'control-label']) !!}
                            {!! Form::text('address', null, ['placeholder'=>trans('dashboard.address'),'class' => 'form-control','requierd']) !!}
                        </div>
                        <div class="col-md-3">
                            {!! Form::label(trans('dashboard.country'), trans('dashboard.country'), ['class' => 'control-label']) !!}
                            {!! Form::select('country_code', $countries , null , ['class' => 'form-control kt-select2','id'=>"kt_select2_3",'requierd']) !!}
                        </div>
                    </div>

                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-6">
                                {!! Form::submit(trans('dashboard.save'), ['class' => 'btn btn-primary']) !!}
                            </div>
                            <div class="col-lg-6 kt-align-right">
                                <button type="reset" class="btn btn-secondary">{{trans('dashboard.cancel')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}

            <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>

@endsection

@push('scripts')
        <script src="{{asset('assets/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>

    <script>

        $('#kt_dropzone_2').dropzone({
            url: "{{route('clients.word_count')}}", // Set the url for your upload script location
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 1,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            acceptedFiles:"application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpeg,.jpg,.png,.gif",
            maxFilesize: 80, // MB
            addRemoveLinks: true,
            timeout: 1800000,
            dictDefaultMessage : "Put your custom message here",
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            init:function () {
                this.on("sending", function(file, xhr, formData){
                    formData.append("source_lang", $("#kt_select2_1").val());
                    formData.append("to_lang", $("#kt_select2_2").val());
                    formData.append('subject_id',$("#kt_select2_3").val());
                });
                this.on("success", function (file, responseText) {
                    $("#words_count").val(responseText.count);
                    $("#price").val(responseText.price);
                    $("#file_path").val(responseText.path);
                    $("#num_of_pages").val(responseText.pages);
                    $("#total_houres").val(responseText.num_of_houres);
                    console.log(responseText);
                });
            }

        });
    </script>
    <script>
        function loadSelectOptions(element, options)
        {
            if(element.prop("tagName") != "SELECT") return;

            element.html('');

            $.each(options, function(key, value)
            {
                element.append('<option value=' + key + '>' + value + '</option>');
            });
            element.prepend('<option value="" selected></option>');
        }
        $("#kt_select2_1").on('change',function () {
            $.ajax({
                /* the route pointing to the post function */
                url: '{{route('clients.get_langs')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: '{{csrf_token()}}', source_lang:this.value},
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) {
                    loadSelectOptions($('#kt_select2_2'),data);
                }
            });
        })

        $("#kt_select2_2").on('change',function () {
            console.log($("#kt_select2_1").val());
            $.ajax({
                /* the route pointing to the post function */
                url: '{{route('clients.get_subjects')}}',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: '{{csrf_token()}}',source_lang:$("#kt_select2_1").val(),to_lang:this.value},
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) {
                    loadSelectOptions($('#kt_select2_3'),data);
                }
            });
        })
    </script>

@endpush
