@extends('clients.layouts.master')
@section('title','View Request')
@push('styles')
    <link href="{{asset('assets/css/pages/invoices/invoice-1.css')}}" rel="stylesheet" type="text/css"/>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-invoice-1">
                        <div class="kt-invoice__head"
                             style="background-image: url({{asset('assets/media/bg/bg-6.jpg')}});">
                            <div class="kt-invoice__container">
                                <div class="kt-invoice__brand">
                                    <h1 class="kt-invoice__title">{{trans('dashboard.status')}} : {{@$client_request->status->name}}</h1>
                                </div>
                                <div class="kt-invoice__items">
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">{{trans('dashboard.date')}}</span>
                                        <span class="kt-invoice__text">{{$client_request->created_at}}</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="kt-invoice__body">
                            <div class="kt-invoice__container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>{{trans('dashboard.source_language')}}</th>
                                            <th>{{trans('dashboard.target_language')}}</th>
                                            <th>{{trans('dashboard.word_count')}}</th>
                                            <th>{{trans('dashboard.sector')}}</th>
                                            <th>{{trans('dashboard.price')}}</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{@$client_request->package->FromLanguage->name}}</td>
                                            <td>{{@$client_request->package->ToLanguage->name}}</td>
                                            <td>{{@$client_request->doc_num_of_words}}</td>
                                            <td>{{@$client_request->subject->name}}</td>
                                            <td>{{$client_request->price}} L.E</td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="kt-invoice__actions">
                            <div class="kt-invoice__container">
                                <a href="{{route('request.download',$client_request->id)}}" class="btn btn-label-brand btn-bold">
                                    {{trans('dashboard.download_file')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{trans('dashboard.download_translated_file')}}
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                    <div class="kt-portlet__body">
                        @if(is_null($client_request->translated_at))
                            {{trans('dashboard.not_translated_yet')}}
                        @else
                            <a href="{{route('request.downloadTranslated',$client_request->id)}}" class="btn btn-label-brand btn-bold">
                                {{trans('dashboard.download_translated_file')}}
                            </a>

                        @endif

                    </div>
                </form>

                <!--end::Form-->
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/pages/crud/file-upload/dropzonejs.js')}}" type="text/javascript"></script>
    <!--end::Page Scripts -->
    <script>
        $('#kt_dropzone_1').dropzone({
            url: "{{route('admin.client_requests.assign',$client_request->id)}}", // Set the url for your upload script location
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 1,
            maxFilesize: 5, // MB
            addRemoveLinks: true,
            autoProcessQueue:false,
            accept: function(file, done) {
                if (file.name == "justinbieber.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            },
            init:function () {
                var submitButton = document.querySelector("#assgin_btn");
                myDropzone = this;
                submitButton.addEventListener("click", function(e) {
                    e.preventDefault();
                    myDropzone.processQueue();

                });
            }
        });
    </script>
@endpush
