@extends('clients.layouts.master')
@section('title','My Requests')
@push('styles')
    @if(app()->getLocale() == "ar")
    <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
    @else
        <link href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />

    @endif

@endpush

@section('content')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    {{trans('dashboard.my_requests')}}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{{route('my_requests.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                            <i class="la la-plus"></i>
                            {{trans('dashboard.order_translation')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_3">
                <thead>
                <tr>
                    <th>{{trans('dashboard.id')}}</th>
                    <th>{{trans('dashboard.language')}}</th>
                    <th>{{trans('dashboard.download_document')}}</th>
                    <th>{{trans('dashboard.number_of_words')}}</th>
                    <th>{{trans('dashboard.price')}}</th>
                    <th>{{trans('dashboard.sector')}}</th>
                    <th>{{trans('dashboard.status')}}</th>
                    <th>{{trans('dashboard.request_at')}}</th>
                    <th>{{trans('dashboard.actions')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($my_requests as $client_request)
                    <tr>
                        <td>{{$client_request->id}}</td>
                        <td>
                           From {{@$client_request->package->FromLanguage->name}} To {{@$client_request->package->ToLanguage->name}}
                        </td>
                        <td>
                            @if(is_null($client_request->translated_at))
                                    Not translated
                            @else
                                <a href="{{route('request.downloadTranslated',$client_request->id)}}" class="btn btn-label-brand btn-bold">
                                    Download Translated File
                                </a>

                            @endif
                        </td>
                        <td>{{$client_request->doc_num_of_words}}</td>
                        <td>{{$client_request->price}}</td>
                        <td>{{@$client_request->subject->name}}</td>
                        <td>{{@$client_request->status->name}}</td>
                        <td>{{$client_request->created_at}}</td>
                        <td>
                            <a href="{{route('my_requests.show',$client_request->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                                <i class="la la-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <!--end: Datatable -->
        </div>
    </div>

@endsection

@push('scripts')
    <!--begin::Page Vendors(used by this page) -->
    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>

    <!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/pages/crud/datatables/data-sources/html.js')}}" type="text/javascript"></script>

    <!--end::Page Scripts -->

@endpush
