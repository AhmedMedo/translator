<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__item kt-menu__item--active" aria-haspopup="true"><a href="{{route('translator.dashboard')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-home"></i><span class="kt-menu__link-text">{{trans('dashboard.dashboard')}}</span></a></li>
            <li class="kt-menu__section ">
                <h4 class="kt-menu__section-text">{{trans('dashboard.settings')}}</h4>
                <i class="kt-menu__section-icon flaticon-more-v2"></i>
            </li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('my_requests.index')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-alert-2"></i><span class="kt-menu__link-text">{{trans('dashboard.client_request')}}</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('clients.change_password')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-alert-2"></i><span class="kt-menu__link-text">{{trans('dashboard.change_password')}}</span></a></li>

            {{--            <li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-users-1"></i><span class="kt-menu__link-text">{{trans('admin/sidebar.clients')}}</span></a></li>--}}

{{--            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-web"></i><span class="kt-menu__link-text">{{trans('admin/sidebar.packages')}}</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>--}}
{{--                <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>--}}
{{--                    <ul class="kt-menu__subnav">--}}
{{--                        <li class="kt-menu__item " aria-haspopup="true"><a href="custom/apps/inbox.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Inbox</span><span class="kt-menu__link-badge"><span class="kt-badge kt-badge--danger kt-badge--inline">new</span></span></a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </li>--}}

        </ul>
    </div>
</div>
