<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

<!-- begin::Head -->

<head>
    <base href="../../../">
    <meta charset="utf-8" />
    <title>Client Area</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Fonts -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{asset('assets/css/pages/login/login-6.css')}}" rel="stylesheet" type="text/css" />

    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{asset('assets/css/skins/header/base/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/brand/dark.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/aside/dark.css')}}" rel="stylesheet" type="text/css" />

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="{{asset('assets/media/favicon.ico')}}" />

    <style type="text/css">
        ul li {
            float: left;
            margin-left: 2em;
            list-style-type:none
        }
        .imgsmall{
            height: 20px;
            width: 20px;
            float: left;
            margin-right: 2px;
            border-radius: 3px;
        }
        .lang_active{
            border-bottom: 2px solid black;
            padding: 2px;
        }
    </style>
    <!-- Hotjar Tracking Code for https://trjim.com/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2267319,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

</head>

<!-- end::Head -->

<!-- begin::Body -->
@php
    if(app()->getLocale() =='en')
    {
        $flag = '226-united-states.svg';
    }
    elseif (app()->getLocale() == 'ar')
    {
        $flag = '151-united-arab-emirates.svg';
    }else{
        $flag = '034-china.svg';

    }
@endphp

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
            <div
                class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
                <div
                    class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                    <div class="block-list">
                        <ul>
                            <li @if(app()->getLocale() =='en') class="lang_active" @endif>
                                <a href="{{LaravelLocalization::getLocalizedURL("en")}}">
                                    <img class="imgsmall" src="{{asset('assets/media/flags/226-united-states.svg')}}" alt="">
                                    English
                                </a>
                            </li>
                            <li @if(app()->getLocale() =='ar') class="lang_active" @endif>
                                <a href="{{LaravelLocalization::getLocalizedURL("ar")}}"">
                                    <img class="imgsmall" src="{{asset('assets/media/flags/151-united-arab-emirates.svg')}}" alt="">
                                    العربية
                                </a>
                            </li>
                            <li @if(app()->getLocale() =='zh') class="lang_active" @endif>
                                <a href="{{LaravelLocalization::getLocalizedURL("zh")}}">
                                    <img class="imgsmall" src="{{asset('assets/media/flags/034-china.svg')}}" alt="">
                                    中文
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="kt-login__wrapper">
                        <div class="kt-login__container">
                            <div class="kt-login__body">
                                <div class="kt-login__logo">
                                    <a href="#">
                                        <img src="{{asset('assets/media/logo_final.png')}}" width="180px"
                                            height="180px">
                                    </a>
                                </div>
                                <div class="kt-login__signin">
                                    <div class="kt-login__head">
                                        <h3 class="kt-login__title">{{trans('login.sign_in')}} </h3>
                                    </div>
                                    <div class="kt-login__form">
                                        <div id="login_errors"></div>

                                        <form class="kt-form" action="">
                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                    placeholder="{{trans('login.email')}}" name="email"
                                                    autocomplete="off">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control form-control-last" type="password"
                                                    placeholder="{{trans('login.password')}}" name="password">
                                            </div>
                                            <div class="kt-login__extra">
                                                <label class="kt-checkbox">
                                                    <input type="checkbox" name="remember">
                                                    {{trans('login.remember_me')}}
                                                    <span></span>
                                                </label>
                                                <a href="javascript:;"
                                                    id="kt_login_forgot">{{trans('login.forget_password')}} ?</a>
                                            </div>
                                            <div class="kt-login__actions">
                                                <button id="kt_login_signin_submit"
                                                    class="btn btn-brand btn-pill btn-elevate">{{trans('login.sign_in')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="kt-login__signup">
                                    <div class="kt-login__head">
                                        <h3 class="kt-login__title">{{trans('login.sign_up')}}</h3>
                                        <div class="kt-login__desc">{{trans('login.details')}}:</div>
                                    </div>
                                    <div class="kt-login__form">
                                        <form class="kt-form" action="">
                                            <div id="errors"></div>
                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                    placeholder="{{trans('login.full_name')}}" name="name">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                    placeholder="{{trans('login.email')}}" name="email"
                                                    autocomplete="off">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" type="password"
                                                    placeholder="{{trans('login.password')}}" name="password">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control form-control-last" type="password"
                                                    placeholder="{{trans('login.confirm_password')}}"
                                                    name="password_confirmation">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                    placeholder="{{trans('login.company_name')}}" name="company_name">
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                    placeholder="{{trans('login.company_website')}}"
                                                    name="company_website">
                                            </div>
                                            <div class="kt-login__extra">
                                                <label class="kt-checkbox">
                                                    <input type="checkbox" name="agree"> {{trans('login.agree')}} <a
                                                        href="#"> {{trans('login.terms')}}</a>.
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="kt-login__actions">
                                                <button id="kt_login_signup_submit"
                                                    class="btn btn-brand btn-pill btn-elevate">{{trans('login.sign_up')}}</button>
                                                <button id="kt_login_signup_cancel"
                                                    class="btn btn-outline-brand btn-pill">{{trans('login.cancel')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="kt-login__forgot">
                                    <div id="forget_errors"></div>
                                    <div class="kt-login__head">
                                        <h3 class="kt-login__title">{{trans('login.forgotten_password')}} ?</h3>
                                        <div class="kt-login__desc">{{trans('login.reset_email')}}</div>
                                    </div>
                                    <div class="kt-login__form">
                                        <form class="kt-form" action="">
                                            <div class="form-group">
                                                <input class="form-control" type="text"
                                                    placeholder="{{trans('login.email')}}" name="email" id="kt_email"
                                                    autocomplete="off">
                                            </div>
                                            <div class="kt-login__actions">
                                                <button id="kt_login_forgot_submit"
                                                    class="btn btn-brand btn-pill btn-elevate">{{trans('login.request')}}</button>
                                                <button id="kt_login_forgot_cancel"
                                                    class="btn btn-outline-brand btn-pill">{{trans('login.cancel')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-login__account">
                            <span class="kt-login__account-msg">
                                {{trans('login.have_account')}} ?
                            </span>&nbsp;&nbsp;
                            <a href="javascript:;" id="kt_login_signup"
                                class="kt-login__account-link">{{trans('login.sign_up')}}!</a>
                        </div>
                    </div>
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content"
                    style="background-image: url({{asset('assets/media/bg/bg-4.jpg')}});">
                    <div class="kt-login__section">
                        <div class="kt-login__block">
                            <h3 class="kt-login__title">Trjim</h3>
                            <div class="kt-login__desc">
                                Your place to translate any thing
                                <br>you want in few steps
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
    </script>

    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>

    <!--end::Global Theme Bundle -->
    <script>
    var config = {
        routes: {
            register: "{{ route('client.register') }}",
            login: "{{route('client.login')}}",
            clients: "{{route('clients.index')}}",
            forget_password: "{{route('client.forget_password')}}"
        }
    };
    </script>
    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/pages/custom/login/login-register.js')}}" type="text/javascript"></script>

    <!--end::Page Scripts -->
</body>

<!-- end::Body -->

</html>
