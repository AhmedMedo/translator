@extends('clients.layouts.master')
@section('title','Change Password')
@push('styles')

@endpush

@section('content')
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Change Password
                        </h3>
                    </div>
                </div>
                @if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <!--begin::Form-->
                {!! Form::open(['route' => 'clients.submit_password', 'method' => 'post','class'=>'kt-form kt-form--label-right']) !!}
                <div class="kt-portlet__body">

                    <div class="form-group row">
                        <div class="col-lg-12">
                            {!! Form::label(trans('login.password'), trans('login.password'), ['class' => 'control-label']) !!}
                                {!! Form::password('password', ['class' => 'form-control' ,'required'=>'required']) !!}

                            @if($errors->has('password'))
                                <div class="invalid-feedback">{{ $errors->first('password') }}.</div>
                            @endif

                        </div>
                        <div class="col-lg-12">
                            {!! Form::label(trans('login.confirm_password'), trans('login.confirm_password'), ['class' => 'control-label']) !!}
                                {!! Form::password('password_confirmation', ['class' => 'form-control' ,'required'=>'required']) !!}

                            @if($errors->has('password_confirmation'))
                                <div class="invalid-feedback">{{ $errors->first('password') }}.</div>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-6">
                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            </div>
                            <div class="col-lg-6 kt-align-right">
                                <button type="reset" class="btn btn-secondary">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}

            <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{asset('assets/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>
@endpush
