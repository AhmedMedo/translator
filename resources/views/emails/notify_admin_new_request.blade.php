<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>

<body style="background: #F6F6F6;padding: 0;margin: 0;font-family: raleway">

	<div class="container" style="background: #fff;max-width: 600px;margin: 30px auto;box-shadow: 1px 1px 10px 0px #8484844f;">
		<div style="padding: 30px 30px 0">
			<img width="100px" height="100px" src="{{asset('assets/media/logo_final.png')}}">
		</div>
		<div style="background: #fff;padding: 30px;">
			<h1 style="font-weight: bold;margin-bottom: 20px;">Request Number #{{$invoice->request_number}}</h1>
			<p style="color: #A7A8A9;text-align: justify;font-size:20px"> New Request sent from {{$invoice->client->name}} , please check</p>
		</div>
		<div style="background: #A1CF5F;padding: 30px;color: #fff;text-align: center">
			<span>Copyright © 2020 Trjim Ltd. All rights reserved.</span>
		</div>
	</div>

</body>
</html>
