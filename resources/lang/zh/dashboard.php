<?php

return [
    'settings'     =>'客戶端設置',
    'country'      =>'國家',
    'address'      => '地址',
    'last_name'    => '姓',
    'first_name'   =>'名字',
    'upload'             =>'將文件放在此處或單擊以上傳',
    'hi'           =>'你好',
    'notifications' => '通知内容',
    'order_translation' => '要求',
    'dashboard'=>'仪表盘 ',
    'client_request' =>'客户要求',
    'change_password' =>'变化密码',
    'order_request'  =>'翻译要求',
    'source_language' =>'源语言',
    'select_source_language' =>'源语言',
    'target_language' => '目标语言',
    'select_target_language' => '目标语言',
    'sector'            =>'专精',
    'word_count'        =>'把文件放在这里或点击上传。',
    'total_price'       => '总价',
    'save'              =>'挽救',
    'cancel'            =>'取消',
    'id'                =>'身分证',
    'my_requests'      => '要求',
    'language'         => '语言 ',
    'download_document' =>'下载文件 ',
    'number_of_words'   => '词数 ',
    'price'             => '价钱',
    'subject'           => '专精',
    'status'            => '状态 ',
    'request_at'        => '要求',
    'actions'           => '程序 ',
    'under_translation' =>'正在进行翻译',
    'date'              => '日期',
    'download_translated_file'     =>'下载翻译文件',
    'download_file'         =>'下载文件',
    'not_translated_yet'     =>'尚未翻译',
    'footer'                =>'Trjim',
    'stats'              => '統計',
    'total_orders'      => '訂單總數',
    'translated_orders' => '總翻譯訂單',
    'total_users'       => '總用戶',
    'under_translated_orders'=>'翻譯下的訂單總數',
    'num_of_pages'  =>'頁數',
    'total_houres'  =>'全部小時數',

];
