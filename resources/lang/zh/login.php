<?php

return [


    'sign_in' => '登入',
    'email' => '电子邮件',
    'password' => '密碼',
    'remember_me' => '密码',
    'forget_password' => '忘记密码',
    'have_account' => '还没有帐号 ?',
    'sign_up'   =>'注册',
    'details'   =>'输入您的详细信息以创建您的帐户',
    'full_name' =>'全名',
    'confirm_password' =>'确认密码',
    'company_name'      => '公司名',
    'company_website'   => '公司网站',
    'agree'             =>'我同意' ,
    'terms'             => '条款和条件',
    'cancel'            =>'取消',
    'forgotten_password' =>'忘记密码',
    'reset_email'        =>'输入您的电子邮件以重置您的密码:',
    'request'           => '请求'


];
