<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin SideBar Langs
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dashboard'         => 'Dashboard',
    'packages'          => 'Packages',
    'client_requests'   =>'Client Requests',
    'translators'       => 'Translator & Agencies',
    'clients'           =>'Clients',
    'subjects'          =>'Subjects'
];
