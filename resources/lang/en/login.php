<?php

return [


    'sign_in' => 'Sign In',
    'email' => 'Email',
    'password' => 'Password',
    'remember_me' => 'Remember me',
    'forget_password' => 'Forget Password',
    'have_account' => 'Don\'t have an account yet ?',
    'sign_up'   =>'Sign Up',
    'details'   =>'Enter your details to create your account',
    'full_name' =>'Full Name',
    'confirm_password' =>'Confirm Password',
    'company_name'      => 'Company Name',
    'company_website'   => 'Company Website',
    'agree'             =>'I agree the' ,
    'terms'             => 'terms and conditions',
    'cancel'            =>'Cancel',
    'forgotten_password' =>'Forgotten Password',
    'reset_email'        =>'Enter your email to reset your password:',
    'request'           => 'Request'


];
