<?php

return [


    'sign_in' => 'تسجيل الدخول',
    'email' => 'البريد الالكتروني',
    'password' => 'كلمة المرور',
    'remember_me' => 'تذكرني',
    'forget_password' => 'نسيت كلمة المرورة',
    'have_account' => 'ليس لديك حساب؟',
    'sign_up'   =>'التسجيل',
    'details'   =>'ادخل بياناتك لاتمام عملية التسجيل',
    'full_name' =>'الاسم كاملا',
    'confirm_password' =>'تاكيد كلمة المرور',
    'company_name'      => 'اسم الشركة',
    'company_website'   => 'رابط الشركة',
    'agree'             =>'أوافق على ' ,
    'terms'             => 'الشروط و الأحكام',
    'cancel'            =>'إلغاء',
    'forgotten_password' =>'استرجاع كلمة المرور',
    'reset_email'        =>'ادخل بريدك الالكتروني لاسترجاع كلمة المرور',
    'request'           => 'طلب'


];
