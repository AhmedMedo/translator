<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Lib\Payment;
use App\Models\ClientTranslationRequest;
use PhpParser\Node\Stmt\Return_;

Route::get('/test',function (\Illuminate\Http\Request $request){
    dd($request->all());
})->name('test');

Route::get('/payment',function (){
//     $payment = new Payment();
//     $red = $payment->CreateOrder(100);
// //    dd($red);
//     return redirect($red);
//     die;

    $invoice = ClientTranslationRequest::find(1);
    return view('emails.invoice',compact('invoice'));

});

Route::get('seen/{id}','NotificationsController@seen')->name('notifications.seen');
Route::get('clients_requests/download/{id}','DownloadController@downloadFile')->name('request.download');
Route::get('clients_requests/downloadTranslated/{id}','DownloadController@downloadTranslated')->name('request.downloadTranslated');

Route::post('webhook','DownloadController@webhook')->name('request.webhook');

Route::get('test_mail',function (){
    $invoice = ClientTranslationRequest::find(1);

    \Mail::send('emails.invoice', ['invoice'=>$invoice], function($message) {
        $message->to('ahmed.medo3061993@gmail.com')->subject('Testing mails');
    });
    \Mail::send('emails.notify_admin_new_request', [], function($message) use ($invoice) {
        $message->to('admin@trjim.com')->subject('New Request');
    });
});

//Admin
Route::group(['prefix'=>'admin','namespace'=>'Admin'],function (){
    //Auth Routes For Admin
    Route::get('login','Auth\LoginController@showLoginForm')->name('admin.showLogin');
    Route::post('login','Auth\LoginController@login')->name('admin.login');
    Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');
    Route::group(['middleware' =>'admin'],function(){

        Route::get('/','DashboardController@index')->name('admin.dashboard');
        Route::resource('packages','PackagesController');
        Route::resource('translators','TranslatorsController');
        Route::resource('subjects','SubjectController');
        Route::resource('users','UsersController');
        //Client Requests
        Route::get('clients_requests/{type?}','ClientsRequestsController@index')->name('admin.client_requests');
        Route::get('clients_requests/show/{id}','ClientsRequestsController@show')->name('admin.client_requests.show');
        Route::post('clients_requests/assign/{id}','ClientsRequestsController@assignTranslator')->name('admin.client_requests.assign');


    });
});


//Translator
Route::group(['prefix'=>'translator','namespace'=>'Translator'],function (){
    //Auth Routes For Admin
    Route::get('login','Auth\LoginController@showLoginForm')->name('translator.showLogin');
    Route::post('login','Auth\LoginController@login')->name('translator.login');
    Route::get('logout', 'Auth\LoginController@logout')->name('translator.logout');



    Route::group(['middleware' =>'translator'],function(){

        Route::get('/','DashboardController@index')->name('translator.dashboard');
        //Client Requests
        Route::get('clients_requests/{type?}','ClientsRequestsController@index')->name('translator.client_requests');
        Route::get('clients_requests/show/{id}','ClientsRequestsController@show')->name('translator.client_requests.show');
        Route::post('upload_translation','ClientsRequestsController@upload_translation')->name('translator.upload_translation');

    });

});


//Client
Route::group(['namespace'=>'Client','prefix' => LaravelLocalization::setLocale(),'middleware' => ['localeSessionRedirect', 'localizationRedirect']],function(){
    Auth::routes(['verify' => true]);

    Route::get('/','Auth\LoginController@showLoginForm');
    Route::post('login','Auth\LoginController@login')->name('client.login');
    Route::post('register','Auth\RegisterController@register')->name('client.register');
    Route::post('forget_password','ForgetPasswordController@SendForgetEmail')->name('client.forget_password');

    Route::get('currency_switch/{lang}','DashboardController@currency_switch')->name('switch_currency');
    Route::group(['middleware' =>'auth:web'],function(){
        Route::get('/logout','Auth\LoginController@logout')->name('client.logout');
        Route::get('clients','DashboardController@index')->name('clients.index');
        Route::get('change_password','ProfileController@ShowChangePassworForm')->name('clients.change_password');
        Route::post('submit_password','ProfileController@submit_password')->name('clients.submit_password');

        Route::resource('my_requests','ClientRequestsController');
        Route::get('show_requests/{type?}','ClientRequestsController@showRequests')->name('clients.show_requests');
        Route::post('get_languages','ClientRequestsController@get_languages')->name('clients.get_langs');
        Route::post('get_subjects','ClientRequestsController@get_subjects')->name('clients.get_subjects');
        Route::post('word_count','ClientRequestsController@word_count')->name('clients.word_count');
    });
});
