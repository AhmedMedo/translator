"use strict";
var KTDatatablesDataSourceHtml = function() {

	var initTable1 = function() {
		var table = $('#kt_table_1');

		// begin first table
		table.DataTable({
			responsive: true,
			columnDefs: [

				{
					targets: 7,
					render: function(data, type, full, meta) {
						var status = {
							0:{'title': 'inactive', 'state': 'danger'},
							1:{'title': 'active', 'state': 'success'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="kt-badge kt-badge--' + status[data].state + ' kt-badge--dot"></span>&nbsp;' +
							'<span class="kt-font-bold kt-font-' + status[data].state + '">' + status[data].title + '</span>';
					},
				},
			],
		});

	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();


var KTDatatablesDataSourceTranslator = function() {

    var initTable2 = function() {
        var table = $('#kt_table_2');

        // begin first table
        table.DataTable({
            responsive: true,
            order:false,
            columnDefs: [

                {
                    targets: 6,
                    render: function(data, type, full, meta) {
                        var status = {
                            0:{'title': 'inactive', 'state': 'danger'},
                            1:{'title': 'active', 'state': 'success'},
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="kt-badge kt-badge--' + status[data].state + ' kt-badge--dot"></span>&nbsp;' +
                            '<span class="kt-font-bold kt-font-' + status[data].state + '">' + status[data].title + '</span>';
                    },
                },
            ],
        });

    };

    return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

var KTDatatablesDataSourceAdminClientRequests = function() {

    var initTable2 = function() {
        var table = $('#kt_table_3');

        // begin first table
        table.DataTable({
            responsive: true,
            order:false,
            // columnDefs: [
            //
            //     {
            //         targets: 5,
            //         render: function(data, type, full, meta) {
            //             var status = {
            //                 0:{'title': 'inactive', 'state': 'danger'},
            //                 1:{'title': 'active', 'state': 'success'},
            //             };
            //             if (typeof status[data] === 'undefined') {
            //                 return data;
            //             }
            //             return '<span class="kt-badge kt-badge--' + status[data].state + ' kt-badge--dot"></span>&nbsp;' +
            //                 '<span class="kt-font-bold kt-font-' + status[data].state + '">' + status[data].title + '</span>';
            //         },
            //     },
            // ],
        });

    };

    return {

        //main function to initiate the module
        init: function() {
            initTable2();
        },

    };

}();

jQuery(document).ready(function() {
	KTDatatablesDataSourceHtml.init();
    KTDatatablesDataSourceTranslator.init();
    KTDatatablesDataSourceAdminClientRequests.init();
});
