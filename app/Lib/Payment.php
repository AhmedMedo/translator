<?php
namespace App\Lib;
use App\Models\Admin;
use App\Notifications;
use GuzzleHttp\Client;
use App\Models\TranslationConfig;
use App\Models\ClientTranslationRequest;
use GuzzleHttp\Exception\RequestException;

class Payment{

    public  $api_key;
    public  $outlet_id;
    public $base_uri;
    public function __construct()
    {
        // $this->api_key = "NTU0ZDkyODEtMTE1Yy00YTE0LTkzNzYtM2JiZjA2NzkyYTZjOjM4YWIyOTEzLWM3OWItNGQ0Ny1iNDk4LWIwNzYxOWFjZmU0Nw==";
        // $this->outlet_id = "32f62972-f5b9-4c73-a0c2-30d3e74f037a";
        // $this->base_uri = "https://api-gateway.sandbox.ngenius-payments.com";

        $this->api_key = "NDBkNzkyZTgtMzUyZi00OTJmLTg2MGQtZjc5NjE4MzU5NmNlOjNiNjBiYTgyLWM3NjctNDBjNi05YWM1LTE2YTE5MTI3MmE0Ng==";
        $this->outlet_id = "fc69fefb-fb7b-44c1-ad47-d315cda730ce";
        $this->base_uri = "https://api-gateway.ngenius-payments.com";

    }

    public function PaymentAccessToken()
    {
        $client = new \GuzzleHttp\Client(['base_uri' => $this->base_uri]);

        try {
           $response =  $client->request('POST', '/identity/auth/access-token',[
                'headers' => [
                    'Authorization'     => 'Basic '.$this->api_key
                ]
            ]);
           if($response->getStatusCode() == 200)
           {
               return json_decode($response->getBody()->getContents())->access_token;
           }
           return false;

        } catch (RequestException $e) {


            return false;

        }

    }

    public  function CreateOrder($amount,$request)
    {
        $access_token = $this->PaymentAccessToken();

        $client = new \GuzzleHttp\Client(['base_uri' => $this->base_uri]);

        try {

            $response =  $client->request('POST', '/transactions/outlets/'.$this->outlet_id.'/orders',[
                'headers' => [
                    'Authorization'     => 'Bearer '.$access_token,
                    'Content-Type'      => 'application/vnd.ni-payment.v2+json',
                    'Accept'            => 'application/vnd.ni-payment.v2+json'
                ],
                'json'=>[
                    'action' =>"SALE",
                    'merchantAttributes'=>[
                        'skipConfirmationPage'=>true,
                        'redirectUrl' =>route('my_requests.index')
                    ],
                    'amount'=> [
                        'currencyCode' => 'AED',
                        'value'         => $amount
                    ],
                    'emailAddress'=>\Auth::user()->email,
                    'billingAddress'=>[
                        'firstName'  => $request->first_name,
                        'lastName'   =>  $request->last_name,
                        'address1'   => $request->address,
                        'countryCode'=> $request->country_code
                    ]
                ]
            ]);

            if($response->getStatusCode() == 201)
            {
                $order = $this->saveOrder($request);
                $data = json_decode($response->getBody()->getContents(),true);
                $order->order_ref = $data['reference'];
                $order->save();

                return $data['_links']['payment']['href'];
            }
            return false;

        } catch (RequestException $e) {

            $error = sprintf('[%s],[%d] ERROR:[%s]', __METHOD__, __LINE__, json_encode($e->getMessage(), true));
            \Log::error($error);

            return false;

        }

    }

    private function saveOrder($request)
    {
        $translation = TranslationConfig::where('from_language_id',$request->from_language_id)
        ->where('to_language_id',$request->to_language_id)
        ->first();
        $translation_request = ClientTranslationRequest::create([
        'client_id'              => \Auth::id(),
        'num_of_pages'           => $request->num_of_pages,
        'total_houres'           => $request->total_houres,
        'translation_package_id' => $translation->id,
        'doc_path'               => $request->file_path,
        'doc_num_of_words'      => $request->file_words_count,
        'price'                 => $request->price ,
        'status_id'             => 1,
        'subject_id'            => $request->subject_id,
        'first_name'            => $request->first_name,
        'last_name'            => $request->last_name,
        'address'               => $request->address,
        'country_code'          => $request->country_code,
        ]);

        //Send Notificaito

        return $translation_request;

    }

}
