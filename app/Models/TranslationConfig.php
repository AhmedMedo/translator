<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranslationConfig extends Model
{
    //
        /**
     * @var string
     */
    protected $table = 'translation_configs';

    /**
     * @var array
     */
    protected $fillable = ['from_language_id','to_language_id','num_of_words_per_page','price_per_page','price_per_page_aed','price_per_page_chinese','active','subject_id'];


    /**
     * From language
     */
    public function FromLanguage()
    {
        return $this->belongsTo('App\Models\Language','from_language_id');
    }




    /**
     * To Language
     */
    public function ToLanguage()
    {
        return $this->belongsTo('App\Models\Language','to_language_id');
    }


    //Subject
    public  function  subject()
    {
        return $this->belongsTo('App\Models\Subject','subject_id');

    }


}
