<?php

namespace App\Models;

use App\Notifications;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Translator extends Authenticatable
{
    //
    protected $fillable = [
        'name', 'email', 'password','active','package_id','percentage'
    ];

    public  function  setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public  function package()
    {
        return $this->belongsTo(TranslationConfig::class,'package_id');
    }

    public  function  notifications()
    {
        return $this->hasMany(Notifications::class,'user_id')->where('type','translator');
    }

    public  function packages()
    {
        return $this->belongsToMany(TranslationConfig::class,'translators_packages','translator_id','translation_config_id');

    }



}
