<?php

namespace App\Models;

use App\Notifications;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    //
     protected $fillable = [
        'name', 'email', 'password','active'
    ];

     public  function  notifications()
     {
         return $this->hasMany(Notifications::class,'user_id')->where('type','admin');
     }

}
