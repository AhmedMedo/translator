<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientTranslationRequest extends Model
{

    /**
     * @var string
     */
    protected $table = 'client_translation_requests';

    /**
     * @var array
     */
    protected $fillable = ['client_id','num_of_pages','total_houres','translation_package_id','doc_path','translated_doc_path','doc_num_of_words','invoice_number','request_number',
                            'price','status_id','is_paid','subject_id','translator_id','translated_at','order_ref','first_name','last_name','address','country_code'];


    public function client()
    {
        return $this->belongsTo('App\User','client_id');
    }

    public function translator()
    {
        return $this->belongsTo(Translator::class,'translator_id');
    }

    public function package()
    {
        return $this->belongsTo(TranslationConfig::class,'translation_package_id');
    }

    public  function  status()
    {
        return $this->belongsTo(Status::class ,'status_id');
    }

    public  function  subject()
    {
        return $this->belongsTo(Subject::class,'subject_id');
    }



    protected static function boot()
    {
        parent::boot();

        static::created(function ($client_request) {

            $requestNumber = (1000 + $client_request->id) . mt_rand(100,999);
            $client_request->update(['request_number' => $requestNumber]);
        });
        static::updated(function ($payment) {


        });

        static::deleted(function ($payment){

        });

    }


}
