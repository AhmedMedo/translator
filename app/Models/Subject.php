<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    //
    protected $fillable =['name','name_ar','name_zh'];

    public function getNameAttribute($value)
    {
        if(app()->getLocale() == 'en')
        {
            return $value;
        }else if (app()->getLocale() == 'ar')
        {
            return $this->name_ar;
        }else if (app()->getLocale() == 'zh')
        {
            return $this->name_zh;
        }else{
            return $value;
        }

    }
}
