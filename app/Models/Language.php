<?php

namespace App\Models;

use App\Models\TranslationConfig;
use Illuminate\Database\Eloquent\Model;
class Language extends Model
{
    //

    public function getNameAttribute($value)
    {
//        return $this->attributes['name'];
        if(app()->getLocale() == 'en')
        {
            return $this->attributes['name'];
        }elseif(app()->getLocale() == 'ar')
        {
            return is_null($this->attributes['name_ar']) ? $this->attributes['name'] : $this->attributes['name_ar'];
        }else{
            return is_null($this->attributes['name_zh']) ? $this->attributes['name'] : $this->attributes['name_zh'];

        }
    }

    public  static  function GetSourceLanguages()
    {
        return TranslationConfig::where('active',1)->with('FromLanguage')->get()->pluck('FromLanguage.name','FromLanguage.id')->toArray();
    }

    public  static  function GetTargetLanguages($source_lang_id = NULL)
    {
        $query = is_null($source_lang_id) ? TranslationConfig::where('active',1) : TranslationConfig::where('active',1) ->where('from_language_id',$source_lang_id);
        return $query->with('ToLanguage')->get()->pluck('ToLanguage.name','ToLanguage.id')->toArray();
    }

    public  static  function GetSubjects($source_lang_id ,$target_language)
    {
        $query = TranslationConfig::where('active',1)->where('from_language_id',$source_lang_id)->where('to_language_id',$target_language);
        return $query->with('subject')->get()->pluck('subject.name','subject.id')->toArray();
    }

}
