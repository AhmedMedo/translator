<?php

namespace App\Library;

use Auth;
use Session;
use App\User;
use App\Models\ClientTranslationRequest;

class Helper

{

    public static function getPagesHoures($from_lang_id , $to_lang_id , $pages)
    {
        $num_of_houres = 3;
        //From English => Arabic  OR Vice Versa
        if(($from_lang_id == 1 && $to_lang_id == 6) || ($from_lang_id == 6 && $to_lang_id == 1))
        {
            if($pages <= 5)
            {
                $num_of_houres = 3;

            }elseif($pages > 5 && $pages <= 10){
                $num_of_houres = 6;
            }elseif($pages > 10 && $pages <= 20)
            {
                $num_of_houres = 10;
            }else{

                $num_of_houres = 24;
            }

        }else{

            if($pages <= 5)
            {
                $num_of_houres = 8;

            }elseif($pages > 5 && $pages <= 10){
                $num_of_houres = 12;
            }elseif($pages > 10 && $pages <= 20)
            {
                $num_of_houres = 24;
            }else{

                $num_of_houres = 48;
            }


        }
        return $num_of_houres;

    }


}
