<?php
namespace App\Library;
use Illuminate\Http\Request;

class FileCounter
{


    public  $file_path;

    public  $extension;


    public  function __construct(\Request $request)
    {
        $this->file_path = $this->uploadFile($request);
        $this->extension = $request->file("file")->extension();
    }

    public  function uploadFile($request)
    {
     return $request->file('file')->store('uploads','public');
    }

    public  function GetPath()
    {
        return $this->file_path;
    }


}
