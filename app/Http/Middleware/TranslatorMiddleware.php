<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class TranslatorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('translator')->check())
        {
            return  redirect()->route('translator.showLogin');
        }

        return $next($request);
    }
}
