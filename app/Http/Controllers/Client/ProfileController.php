<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //
    public  function  ShowChangePassworForm()
    {
        return view('clients.profile.change_password');
    }

    public  function submit_password(Request $request)
    {
        $request->validate(['password'=>'required|confirmed']);

        $user = auth()->user();
        $user->password = \Hash::make($request->password);
        $user->save();

        return redirect()->back()->with('success','Your Request has been added');



    }


}
