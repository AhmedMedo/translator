<?php

namespace App\Http\Controllers\Client;

use Auth;
use App\Lib\Payment;
use App\Models\Admin;
use App\Notifications;
use App\Models\Country;
use App\Models\Subject;
use App\Models\Language;
use Spatie\PdfToText\Pdf;
use Illuminate\Http\Request;
use App\Models\TranslationConfig;
use App\Http\Controllers\Controller;
use App\Models\ClientTranslationRequest;

class ClientRequestsController extends Controller
{

    public function showRequests(Request $request , $type=NULL)
    {
        $my_requests = Auth::user()->TranslationRequests()->where('is_paid',1)->orderBy('created_at','DESC');
        if(is_null($type))
        {
            $my_requests = $my_requests->get();
        }else if ($type == 'under_translation')
        {
            $my_requests = $my_requests->where('status_id',4)->get();
        } else if ($type == 'finished')
        {
            $my_requests = $my_requests->where('status_id',5)->get();
        }

        return view('clients.requests.index',compact('my_requests'));

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $my_requests = Auth::user()->TranslationRequests()->where('is_paid',1)->orderBy('created_at','DESC')->get();

        if($request->has('ref'))
        {
            $invoice = ClientTranslationRequest::where('order_ref',$request->input('ref'))->first();
            if(is_object($invoice))
            {

                $admins = Admin::all();
                foreach ($admins as $admin)
                {
                    Notifications::create([
                    'user_id'=>$admin->id,
                    'type'   =>'admin',
                    'message' => \Auth::user()->name . ' sent a translation request to be handeled',
                    'item_id' => $invoice->id
                    ]);
                }

                $invoice->is_paid = 1;
                $invoice->save();

                try{

                    \Mail::send('emails.invoice', ['invoice'=>$invoice], function($message) use ($invoice) {
                        $message->to($invoice->client->email)->subject('Invoice');
                    });

                    \Mail::send('emails.notify_admin_new_request', ['invoice'=>$invoice], function($message) use ($invoice) {
                        $message->to('admin@trjim.com')->subject('New Request');
                    });

                }catch(\Exception $e){

                }
                \Session::flash('success', 'Your Payment Success');
            }

        }
        return view('clients.requests.index',compact('my_requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $source_languages = Language::GetSourceLanguages();
        $target_languages = Language::GetTargetLanguages();
        $source_languages['']='select';
        $subjects = Subject::all()->pluck('name','id')->toArray();
        $countries = Country::all()->pluck('name','code')->toArray();
        return view('clients.requests.form',compact('source_languages','target_languages','subjects','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'file_path' => 'required',
        ]);

        $payment = new Payment();
        $redirect = $payment->CreateOrder(round($request->price,2)*100,$request);
        return redirect($redirect);

    //    return redirect()->route('my_requests.index')->with('success','Your Request has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $client_request = ClientTranslationRequest::find($id);

        return view('clients.requests.view',compact('client_request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Word Count
     */
    public  function word_count(Request $request)
    {

//        dd($request->file('file')->extension());
        $ext = $request->file('file')->extension();
        $file_words=0;

        $path = $request->file('file')->store('uploads','public');
        $full_path = public_path('storage'.DIRECTORY_SEPARATOR.$path);

        if($ext =='docx')
        {
            $striped_content = '';
            $content = '';

            $zip = zip_open($full_path);

            while ($zip_entry = zip_read($zip)) {

                if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

                if (zip_entry_name($zip_entry) != "word/document.xml") continue;

                $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

                zip_entry_close($zip_entry);
            }

            zip_close($zip);
            $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
            $content = str_replace('</w:r></w:p>', "<br>", $content);
            $striped_content = strip_tags($content);
            $file_words = count(array_filter(explode(' ',$striped_content)));
        }elseif($ext =='pdf'){
//            $x = 'c:/Program Files/Git/mingw64/bin/pdftotext';
            setlocale(LC_ALL, 'ar_AE.utf8');

            $parser = new \Smalot\PdfParser\Parser();
            $pdf    = $parser->parseFile($full_path);
            $text = $pdf->getText();
            // $text_array = explode(' ',$text);
            // $file_words = count($text_array);
           $file_words = count(array_filter(explode(' ',$text)));

//            $text =  Pdf::getText($full_path); //returns the text from the pdf
//            $lines = str_word_count($text, 1);
//            $file_words = count($lines);

        }elseif(in_array($ext,['png','jpg','jpeg','gif']))
        {
            //OCR
            $text = \OCR::scan($full_path);
            $file_words = count(array_filter(explode(' ',$text)));

        }

        //price
        $translation = TranslationConfig::where('from_language_id',$request->source_lang)
                                        ->where('to_language_id',$request->to_lang)
                                        ->first();
        if($file_words <= $translation->num_of_words_per_page)
        {
            $pages = 1;
        }else{
            $pages = $file_words/$translation->num_of_words_per_page;

        }


        // if(\Session::get('currency') =='usd')
        // {
        //     $price = ceil($pages) * $translation->price_per_page;
        // }
        // elseif (\Session::get('currency') == 'aed')
        // {
        //     $price = ceil($pages) * $translation->price_per_page_aed;
        // }else{
        //     $price = ceil($pages) * $translation->price_per_page_chinese;
        // }
        $pages = round($pages, 0, PHP_ROUND_HALF_DOWN);
        $price = $pages * $translation->price_per_page_aed;

        $num_of_houres = \App\Library\Helper::getPagesHoures($request->source_lang ,$request->to_lang,$pages );

        return response()->json(['status'=>1,'count'=>$file_words,'price' => round($price,2),'path'=>$path,'pages'=>$pages,'num_of_houres'=>$num_of_houres]);
    }

    /**
     * Get Langs
     */
    public function get_languages(Request $request)
    {

        $langs = Language::GetTargetLanguages($request->source_lang);
        return response()->json($langs);

    }

    public function get_subjects(Request $request)
    {

        $subjects = Language::GetSubjects($request->source_lang,$request->to_lang);
        return response()->json($subjects);

    }




}
