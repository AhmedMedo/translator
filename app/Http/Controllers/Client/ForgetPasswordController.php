<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use Mail;
use App\Mail\ForgetPasswordMail;

class ForgetPasswordController extends Controller
{
    //
    public  function SendForgetEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|string|exists:users,email',
        ]);

        $user =User::whereEmail($request->email)->first();
        $password = Str::random(8);
        $user->password = \Hash::make($password);
        $user->save();

        Mail::to($user->email)->send(new ForgetPasswordMail($user,$password));


    }
}
