<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Lib\Payment;
use App\Models\ClientTranslationRequest;
use Facade\FlareClient\Http\Client;
use Illuminate\Http\Request;
use Session;
use Auth;
class DashboardController extends Controller
{
    //
    public  function index()
    {
        $data['total_orders'] = ClientTranslationRequest::where('client_id',Auth::user()->id)->where('is_paid',1)->count();

        $data['under_translations'] = ClientTranslationRequest::where('status_id',4)->where('client_id',Auth::user()->id)->where('is_paid',1)->count();

        $data['translated_orders'] = ClientTranslationRequest::where('status_id',5)->where('client_id',Auth::user()->id)->where('is_paid',1)->count();

        $data['total_paid'] = ClientTranslationRequest::where('status_id',5)->where('is_paid',1)->where('client_id',Auth::user()->id)->sum('price');


        return view('clients.dashboard',$data);
    }

    public function currency_switch($lang)
    {
        Session::put('currency',$lang);
        return redirect()->back();
    }


}
