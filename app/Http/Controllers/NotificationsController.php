<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications;
use Mockery\Matcher\Not;

class NotificationsController extends Controller
{
    //

    public  function seen($id)
    {
        $notification = Notifications::find($id);
        $notification->seen = 1;
        $notification->save();
        if(\Auth::guard('admin')->check())
        {
            return redirect()->route('admin.client_requests.show',$notification->item_id);

        }elseif(\Auth::guard('translator')->check())
        {
            return redirect()->route('translator.client_requests.show',$notification->item_id);

        }else{
            return redirect()->route('my_requests.show',$notification->item_id);

        }




    }

}
