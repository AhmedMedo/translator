<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ClientTranslationRequest;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    /**
     *
     */
    public  function  index()
    {
        //Statiscits
        $data['total_orders']=ClientTranslationRequest::count();

        $data['translated_orders'] = ClientTranslationRequest::where('status_id',5)->count();

        $data['total_users']=User::count();

        $data['under_translations'] = ClientTranslationRequest::where('status_id',4)->count();

        return view('admin.dashboard',$data);
    }

}
