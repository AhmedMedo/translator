<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Translator;
use App\Notifications;
use Illuminate\Http\Request;
use App\Models\ClientTranslationRequest;
class ClientsRequestsController extends Controller
{
    //
    /**
     * List All Client Requests
     */
    public  function  index(Request $request , $type=NULL)
    {
        $client_requests = ClientTranslationRequest::query()->where('is_paid',1)->orderBy('created_at','DESC');

        if(is_null($type))
        {
            $client_requests = $client_requests->get();
        }else if ($type == 'under_translation')
        {
            $client_requests = $client_requests->where('status_id',4)->get();
        } else if ($type == 'finished')
        {
            $client_requests = $client_requests->where('status_id',5)->get();
        }

        return view('admin.client_requests.index',compact('client_requests'));
    }

    /**
     * Show Client Request
     */
    public function show($id)
    {

        $client_request = ClientTranslationRequest::find($id);

        $translators = Translator::whereHas('packages', function($q) use ($client_request){
            $q->where('translation_config_id',$client_request->translation_package_id);
        })->get();
        return view('admin.client_requests.view',compact('client_request','translators'));
    }

    public  function assignTranslator(Request $request ,$id)
    {
        $translator = Translator::find($request->translator_id);
        $client_request = ClientTranslationRequest::find($id);
        $client_request->translator_id = $request->translator_id;
        $client_request->status_id = 4; //UnderTranslation
        $client_request->translator_commission = ($translator->percentage/100) * $client_request->price;
        $client_request->save();

        //Notifiy Translator
        Notifications::create([
            'user_id'=>$request->translator_id,
            'type'   =>'translator',
            'message' => \Auth::guard('admin')->user()->name . ' assigned you to translate this file',
            'item_id' => $client_request->id
        ]);


        //Send Email To Translator

        try{

            $doc_path = public_path('storage/'.$client_request->doc_path);
            \Mail::send('emails.assign_translator', ['invoice'=>$client_request], function($message) use ($client_request,$doc_path) {
                $message->to($client_request->translator->email)->subject('Translate Request');
                $message->attach($doc_path);
            });

        }catch(\Exception $e){

        }

        return redirect()->route('admin.client_requests')->with('success','Translator Assginded');

    }

    public  function downloadFile($id)
    {
        $client_request = ClientTranslationRequest::find($id);

        return response()->download(public_path('storage/'.$client_request->doc_path));

    }




}
