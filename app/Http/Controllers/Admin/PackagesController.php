<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TranslationConfig;
use App\Models\Language;
use App\Models\Subject;
class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $packages = TranslationConfig::all();
        return view('admin.packages.index',compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $langauges = Language::all()->pluck('name','id')->toArray();
        $subjects = Subject::all()->pluck('name','id')->toArray();
        return view('admin.packages.form',compact('langauges','subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request_data = $request->only('from_language_id','to_language_id','num_of_words_per_page','price_per_page','price_per_page_aed','price_per_page_chinese','subject_id');
        if($request->has('active'))
        {
            $request_data['active'] = 1;
        }else{
            $request_data['active'] = 0;
        }
        //If the two lanugage Pair Exist Update Else Add New One
        $check_package =TranslationConfig::where('from_language_id',$request_data['from_language_id'])
                                          ->where('to_language_id',$request_data['to_language_id'])
                                          ->where('subject_id',$request_data['subject_id'])
                                          ->first();

        //TODO:Add Auth Guard Admin For Package

        if(is_null($check_package))
        {
            //Create New One
            TranslationConfig::create($request_data);
            return redirect()->route('packages.index')->with('success','Pacakge Added');

        }else{
            //Update
            $check_package->update($request_data);
            return redirect()->route('packages.index')->with('success','Pacakge updated as the two lanuage pairs already exists');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $package = TranslationConfig::find($id);
        $langauges = Language::all()->pluck('name','id')->toArray();
        $subjects = Subject::all()->pluck('name','id')->toArray();

        return  view('admin.packages.form',compact('package','langauges','subjects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $package = TranslationConfig::find($id);
        $request_data = $request->only('from_language_id','to_language_id','num_of_words_per_page','price_per_page','price_per_page_aed','price_per_page_chinese','subject_id');
        if($request->has('active'))
        {
            $request_data['active'] = 1;
        }else{
            $request_data['active'] = 0;
        }
        $package->update($request_data);
        return redirect()->route('packages.index')->with('success','Pacakge Updated');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        TranslationConfig::find($id)->delete();
        return redirect()->route('packages.index')->with('deleted','Pacakge deleted');

    }
}
