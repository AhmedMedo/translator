<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TranslationConfig;
use App\Models\Translator;
use Illuminate\Http\Request;

class TranslatorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $translators = Translator::all();
        return view('admin.translators.index',compact('translators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $pairs = TranslationConfig::where('active',1)->get();
        return  view('admin.translators.form',compact('pairs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(['email' => 'required|email|unique:translators,email','password'=>'required|confirmed']);
        $request_data = $request->only('name','email','password','percentage');
        if($request->has('active'))
        {
            $request_data['active'] = 1;
        }else{
            $request_data['active'] = 0;
        }
       $translator = Translator::create($request_data);
//        dd($translator);

        $translator->packages()->attach($request->packages);
        return redirect()->route('translators.index')->with('success','Translator Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $translator = Translator::find($id);
        $pairs = TranslationConfig::where('active',1)->get();

        return  view('admin.translators.form',compact('translator','pairs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $translator = Translator::find($id);
        if($request->has('password') && $request->password != NULL)
        {
            $request->validate(['password'=>'required|confirmed']);
            $request_data = $request->only('name','password','email','percentage');
        }else{
            $request_data = $request->only('name','email','percentage');

        }


        if($request->has('active'))
        {
            $request_data['active'] = 1;
        }else{
            $request_data['active'] = 0;
        }
        $translator->update($request_data);
        $translator->packages()->sync($request->packages);

        return redirect()->route('translators.index')->with('success','Translator Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Translator::find($id)->delete();
        return redirect()->route('translators.index')->with('deleted','Translator Deleted');

    }
}
