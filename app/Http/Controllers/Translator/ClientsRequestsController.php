<?php

namespace App\Http\Controllers\Translator;

use App\Http\Controllers\Controller;
use App\Models\ClientTranslationRequest;
use App\Models\Translator;
use App\Notifications;
use Illuminate\Http\Request;

class ClientsRequestsController extends Controller
{
    //
    public  function  index(Request $request , $type=NULL)
    {
        $client_requests = ClientTranslationRequest::where('translator_id',\Auth::guard('translator')->user()->id)->where('is_paid',1)->orderBy('created_at','DESC');

        if(is_null($type))
        {
            $client_requests = $client_requests->get();
        }else if ($type == 'under_translation')
        {
            $client_requests = $client_requests->where('status_id',4)->get();
        } else if ($type == 'finished')
        {
            $client_requests = $client_requests->where('status_id',5)->get();
        }
        return view('translator.client_requests.index',compact('client_requests'));
    }

    /**
     * Show Client Request
     */
    public function show($id)
    {

        $client_request = ClientTranslationRequest::find($id);
        return view('translator.client_requests.view',compact('client_request'));
    }

    /**
     * Upload Translation
     */
    public function upload_translation(Request $request)
    {
        //Upload File
        $path = $request->file('file')->store('uploads','public');
        //Change Status and update
        $client_request = ClientTranslationRequest::find($request->request_id);
        $client_request->status_id = 5; //Finished
        $client_request->translated_doc_path=$path;
        $client_request->translated_at=\Carbon\Carbon::now()->toDateString();
        $client_request->save();

        //Notify Client
        Notifications::create([
            'user_id'=>$client_request->client_id,
            'type'   =>'user',
            'message' => 'Your request has been translated',
            'item_id' => $client_request->id
        ]);


        //Send Mail To Client About the Translated File
        try{
            $doc_path = public_path('storage/'.$client_request->translated_doc_path);
            \Mail::send('emails.notify_client', ['invoice'=>$client_request], function($message) use ($client_request,$doc_path) {
                $message->to($client_request->client->email)->subject('Translation Finished');
                $message->attach($doc_path);
            });

        }catch(\Exception $e){

        }


    }




}
