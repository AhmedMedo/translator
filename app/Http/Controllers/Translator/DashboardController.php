<?php

namespace App\Http\Controllers\Translator;

use App\Http\Controllers\Controller;
use App\Models\ClientTranslationRequest;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    public  function index()
    {
        $data['translated_orders'] = ClientTranslationRequest::where('translator_id',\Auth::guard('translator')->user()->id)->where('status_id',5)->count();
        $data['under_translations'] = ClientTranslationRequest::where('translator_id',\Auth::guard('translator')->user()->id)->whereIn('status_id',[2,4])->count();
        $data['commesion']          = ClientTranslationRequest::where('translator_id',\Auth::guard('translator')->user()->id)->sum('translator_commission');
        return view('translator.dashboard',$data);
    }

}
