<?php

namespace App\Http\Controllers;

use App\Models\ClientTranslationRequest;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    //

    public  function downloadFile($id)
    {
        $client_request = ClientTranslationRequest::find($id);

        return response()->download(public_path('storage/'.$client_request->doc_path));

    }
    public  function downloadTranslated($id)
    {
        $client_request = ClientTranslationRequest::find($id);
        return response()->download(public_path('storage/'.$client_request->translated_doc_path));
    }

    public function webhook(Request $request)
    {
        $request_json = json_encode($request->all());
        \Log::info($request_json);

    }

}
